def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(nums)
  nums.reduce(:+) || 0
end

def multiply(*nums)
  nums.reduce(:*)
end

def factorial(num)
  return 1 if num <= 1
  num * factorial(num - 1)
end

def power(a, b)
  a**b
end
