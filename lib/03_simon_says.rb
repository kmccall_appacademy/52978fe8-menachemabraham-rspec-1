LITTLE_WORDS = %w(a an the and but or for nor on at to from by over)

def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times = 2)
  Array.new(times, str).join(" ")
end

def start_of_word(word, length)
  word[0, length]
end

def first_word(str)
  str.split.first
end

def titleize(str)
  str.capitalize.split.map { |word| titleize_word(word) }.join(" ")
end

def titleize_word(word)
  LITTLE_WORDS.include?(word) ? word : word.capitalize
end
