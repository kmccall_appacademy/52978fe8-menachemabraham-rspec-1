def translate(str)
  str.gsub(/[a-zA-Z]+/) { |word| translate_word(word) }
end

def translate_word(word)
  translated_word = "#{vowel_segment(word)}#{pre_vowel_segment(word)}ay"
  word[0] == word[0].upcase ? translated_word.capitalize : translated_word
end

def pre_vowel_segment(word)
  word[0...first_vowel_index(word)]
end

def vowel_segment(word)
  word[first_vowel_index(word)..-1]
end

def first_vowel_index(word)
  idx = word.index(/[aeiouAEIOU]/)
  word[idx-1, 2] == 'qu' ? idx += 1 : idx
end
